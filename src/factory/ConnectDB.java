package factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MN-LEO
 */
public class ConnectDB {

    private static Connection con;

    public static Connection getConnection() {
        Connection _con = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");

            String serverName = "localhost";
            String dbName = "cdm1final";
            String userName = "root";
            String pass = "";

            _con = DriverManager.getConnection("jdbc:mysql://" + serverName + "/" + dbName, userName, pass);

        } catch (ClassNotFoundException e) {
            System.out.println("error: " + e);
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        }

        con = _con;
        return _con;
    }

    public static void close() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                System.out.println("error: " + ex);
            }
        }
    }

}

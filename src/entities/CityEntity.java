package entities;

/**
 *
 * @author MN-LEO
 */
public class CityEntity {

    private int id;
    private String name;
    private String state;
    
    public CityEntity() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
}

package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author Leandro Pinheiro
 */
public class Menu extends JMenuBar {
    
    private JMenu registers;
    private JMenu reports;
    private JMenuItem student;
    private JMenuItem city;
    private JMenuItem exit;
    private JMenuItem students;
    private JMenuItem cities;
    
    public Menu(Main main) {
        registers = new JMenu("Cadastro");
        reports = new JMenu("Relatório");
        
        student = new JMenuItem("Aluno");
        city = new JMenuItem("Cidade");
        exit = new JMenuItem("Sair");
        students = new JMenuItem("Alunos");
        cities = new JMenuItem("Cidades");
        
        add(registers);
        add(reports);
        
        registers.add(student);
        registers.add(city);
        registers.add(exit);
        reports.add(students);
        reports.add(cities);
        
        student.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                main.menuHandler("student");
            }
        });
        
        city.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                main.menuHandler("city");
            }
        });
        
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                main.menuHandler("exit");
            }
        });
        
        students.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                main.menuHandler("students");
            }
        });
        
        cities.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                main.menuHandler("cities");
            }
        });
    }
}

package gui;

import dao.CitiesDAO;
import entities.CityEntity;
import javax.swing.JOptionPane;

/**
 *
 * @author MN-LEO
 */
public class City extends javax.swing.JDialog {

    private CitiesDAO citiesDAO;

    public City(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(parent);

        citiesDAO = new CitiesDAO();

        CityEntity student = citiesDAO.nav("first");
        if (student != null) {
            showData(student);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lCod = new javax.swing.JLabel();
        lName = new javax.swing.JLabel();
        lLastname = new javax.swing.JLabel();
        tfCod = new javax.swing.JTextField();
        tfCity = new javax.swing.JTextField();
        tfState = new javax.swing.JTextField();
        btSearch = new javax.swing.JButton();
        btFirst = new javax.swing.JButton();
        btPrev = new javax.swing.JButton();
        btNext = new javax.swing.JButton();
        btLast = new javax.swing.JButton();
        btNew = new javax.swing.JButton();
        btSave = new javax.swing.JButton();
        btDel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lCod.setText("Cód.:");

        lName.setText("Cidade:");

        lLastname.setText("Estado:");

        tfCod.setEditable(false);

        btSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/images/iconSearch.png"))); // NOI18N
        btSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSearchActionPerformed(evt);
            }
        });

        btFirst.setText("<<");
        btFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFirstActionPerformed(evt);
            }
        });

        btPrev.setText("<");
        btPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btPrevActionPerformed(evt);
            }
        });

        btNext.setText(">");
        btNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNextActionPerformed(evt);
            }
        });

        btLast.setText(">>");
        btLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLastActionPerformed(evt);
            }
        });

        btNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/images/iconAdd.png"))); // NOI18N
        btNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNewActionPerformed(evt);
            }
        });

        btSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/images/iconSave.png"))); // NOI18N
        btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSaveActionPerformed(evt);
            }
        });

        btDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/images/iconDel.png"))); // NOI18N
        btDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lCod)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfCod)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btSearch))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lLastname)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfState))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfCity))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btFirst)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btPrev)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btNext)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btLast))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btNew)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btSave)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btDel)))
                        .addGap(0, 172, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lCod)
                    .addComponent(tfCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btSearch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lName)
                    .addComponent(tfCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lLastname)
                    .addComponent(tfState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btFirst)
                    .addComponent(btPrev)
                    .addComponent(btNext)
                    .addComponent(btLast))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btNew)
                    .addComponent(btSave)
                    .addComponent(btDel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSearchActionPerformed
        String codSearch = JOptionPane.showInputDialog(this, "Digite o código a pesquisar");

        if (!codSearch.equals("") && codSearch.matches("^[0-9]*$")) {
            CityEntity city = citiesDAO.getByID(Integer.parseInt(codSearch));
            if (city != null) {
                tfCod.setText(String.valueOf(city.getId()));
                tfCity.setText(city.getName());
                tfState.setText(city.getState());
            }
        }
    }//GEN-LAST:event_btSearchActionPerformed

    private void btFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFirstActionPerformed
        CityEntity student = citiesDAO.nav("first");
        if (student != null) {
            showData(student);
        }
    }//GEN-LAST:event_btFirstActionPerformed

    private void btPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btPrevActionPerformed
        CityEntity student = citiesDAO.nav("prev");
        if (student != null) {
            showData(student);
        }
    }//GEN-LAST:event_btPrevActionPerformed

    private void btNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNextActionPerformed
        CityEntity student = citiesDAO.nav("next");
        if (student != null) {
            showData(student);
        }
    }//GEN-LAST:event_btNextActionPerformed

    private void btLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLastActionPerformed
        CityEntity student = citiesDAO.nav("last");
        if (student != null) {
            showData(student);
        }
    }//GEN-LAST:event_btLastActionPerformed

    private void btNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNewActionPerformed
        tfCod.setText("");
        tfCity.setText("");
        tfState.setText("");
    }//GEN-LAST:event_btNewActionPerformed

    private void btSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSaveActionPerformed
        CityEntity city = new CityEntity();
        if (!tfCod.getText().equals("")) {
            city.setId(Integer.parseInt(tfCod.getText()));
        }
        city.setName(tfCity.getText());
        city.setState(tfState.getText());

        int result;
        String msg = "Efetuado";
        if (city.getId() == 0) {
            result = citiesDAO.save(city);
        } else {
            msg = "Atualizado";
            result = citiesDAO.update(city);
        }

        if (result > 0) {
            JOptionPane.showMessageDialog(this, "Cadastro " + msg + " com sucesso!");
        } else {
            JOptionPane.showMessageDialog(this, "Erro ao tentar cadastrar!");
        }
    }//GEN-LAST:event_btSaveActionPerformed

    private void btDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDelActionPerformed
        if (!tfCod.getText().equals("")) {
            int result = citiesDAO.delete(Integer.parseInt(tfCod.getText()));

            if (result > 0) {
                JOptionPane.showMessageDialog(this, "Cadastro deletado com sucesso!");
                tfCod.setText("");
                tfCity.setText("");
                tfState.setText("");
            } else {
                JOptionPane.showMessageDialog(this, "Erro ao tentar deletar!");
            }
        }
    }//GEN-LAST:event_btDelActionPerformed

    private void showData(CityEntity city) {
        tfCod.setText(String.valueOf(city.getId()));
        tfCity.setText(city.getName());
        tfState.setText(city.getState());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                City dialog = new City(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btDel;
    private javax.swing.JButton btFirst;
    private javax.swing.JButton btLast;
    private javax.swing.JButton btNew;
    private javax.swing.JButton btNext;
    private javax.swing.JButton btPrev;
    private javax.swing.JButton btSave;
    private javax.swing.JButton btSearch;
    private javax.swing.JLabel lCod;
    private javax.swing.JLabel lLastname;
    private javax.swing.JLabel lName;
    private javax.swing.JTextField tfCity;
    private javax.swing.JTextField tfCod;
    private javax.swing.JTextField tfState;
    // End of variables declaration//GEN-END:variables
}

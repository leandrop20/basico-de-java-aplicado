package gui;

import dao.CitiesDAO;
import dao.StudentsDAO;
import entities.CityEntity;
import entities.StudentEntity;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author MN-LEO
 */
public class Student extends javax.swing.JDialog {

    private StudentsDAO studentsDAO;
    private CitiesDAO citiesDAO;

    public Student(java.awt.Frame parent, boolean modal) {
        super(parent, "Aluno", modal);
        initComponents();
        setLocationRelativeTo(parent);

        studentsDAO = new StudentsDAO();
        citiesDAO = new CitiesDAO();

        ArrayList<CityEntity> cities = citiesDAO.getAllCities();
        for (CityEntity city : cities) {
            tfCity.addItem(city.getName());
        }

        StudentEntity student = studentsDAO.nav("first");
        if (student != null) {
            showData(student);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lCod = new javax.swing.JLabel();
        lName = new javax.swing.JLabel();
        lLastname = new javax.swing.JLabel();
        lCity = new javax.swing.JLabel();
        tfCity = new javax.swing.JComboBox<>();
        tfCod = new javax.swing.JTextField();
        tfName = new javax.swing.JTextField();
        tfLastname = new javax.swing.JTextField();
        btSearch = new javax.swing.JButton();
        btFirst = new javax.swing.JButton();
        btPrev = new javax.swing.JButton();
        btNext = new javax.swing.JButton();
        btLast = new javax.swing.JButton();
        btNew = new javax.swing.JButton();
        btSave = new javax.swing.JButton();
        btDel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lCod.setText("Cód.:");

        lName.setText("Nome:");

        lLastname.setText("Sobrenome:");

        lCity.setText("Cidade:");

        tfCod.setEditable(false);

        btSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/images/iconSearch.png"))); // NOI18N
        btSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSearchActionPerformed(evt);
            }
        });

        btFirst.setText("<<");
        btFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFirstActionPerformed(evt);
            }
        });

        btPrev.setText("<");
        btPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btPrevActionPerformed(evt);
            }
        });

        btNext.setText(">");
        btNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNextActionPerformed(evt);
            }
        });

        btLast.setText(">>");
        btLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLastActionPerformed(evt);
            }
        });

        btNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/images/iconAdd.png"))); // NOI18N
        btNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNewActionPerformed(evt);
            }
        });

        btSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/images/iconSave.png"))); // NOI18N
        btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSaveActionPerformed(evt);
            }
        });

        btDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/images/iconDel.png"))); // NOI18N
        btDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lCod)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfCod)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btSearch)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lLastname)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfLastname))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lName)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfName)))
                        .addGap(6, 6, 6))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btFirst)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btPrev)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btNext)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btLast))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btNew)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btSave)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btDel)))
                        .addGap(0, 178, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lCity)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfCity, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btSearch)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lCod)
                        .addComponent(tfCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lName)
                    .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lLastname)
                    .addComponent(tfLastname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lCity)
                    .addComponent(tfCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btFirst)
                    .addComponent(btPrev)
                    .addComponent(btNext)
                    .addComponent(btLast))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btNew)
                    .addComponent(btSave)
                    .addComponent(btDel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSearchActionPerformed
        String codSearch = JOptionPane.showInputDialog(this, "Digite o ID a pesquisar!");

        if (!codSearch.equals("") && codSearch.matches("^[0-9]*$")) {
            StudentEntity student = studentsDAO.getByID(Integer.parseInt(codSearch));
            if (student != null) {
                tfCod.setText(String.valueOf(student.getId()));
                tfName.setText(student.getName());
                tfLastname.setText(student.getLastname());
                tfCity.setSelectedItem(citiesDAO.getByID(student.getCityID()).getName());
            }
        }
    }//GEN-LAST:event_btSearchActionPerformed

    private void btFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFirstActionPerformed
        StudentEntity student = studentsDAO.nav("first");
        if (student != null) {
            showData(student);
        }
    }//GEN-LAST:event_btFirstActionPerformed

    private void btPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btPrevActionPerformed
        StudentEntity student = studentsDAO.nav("prev");
        if (student != null) {
            showData(student);
        }
    }//GEN-LAST:event_btPrevActionPerformed

    private void btNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNextActionPerformed
        StudentEntity student = studentsDAO.nav("next");
        if (student != null) {
            showData(student);
        }
    }//GEN-LAST:event_btNextActionPerformed

    private void btLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLastActionPerformed
        StudentEntity student = studentsDAO.nav("last");
        if (student != null) {
            showData(student);
        }
    }//GEN-LAST:event_btLastActionPerformed

    private void btNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNewActionPerformed
        tfCod.setText("");
        tfName.setText("");
        tfLastname.setText("");
        tfCity.setSelectedIndex(0);
    }//GEN-LAST:event_btNewActionPerformed

    private void btSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSaveActionPerformed
        StudentEntity student = new StudentEntity();
        if (!tfCod.getText().equals("")) {
            student.setId(Integer.parseInt(tfCod.getText()));
        }
        student.setName(tfName.getText());
        student.setLastname(tfLastname.getText());
        student.setCityID(citiesDAO.getIDByName(tfCity.getSelectedItem().toString()));

        int result;
        String msg = "Efetuado";
        if (student.getId() == 0) {
            result = studentsDAO.save(student);
        } else {
            msg = "Atualizado";
            result = studentsDAO.update(student);
        }

        if (result > 0) {
            JOptionPane.showMessageDialog(this, "Cadastro " + msg + " com sucesso!");
        } else {
            JOptionPane.showMessageDialog(this, "Erro ao tentar cadastrar!");
        }
    }//GEN-LAST:event_btSaveActionPerformed

    private void btDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDelActionPerformed
        if (!tfCod.getText().equals("")) {
            int result = studentsDAO.delete(Integer.parseInt(tfCod.getText()));

            if (result > 0) {
                JOptionPane.showMessageDialog(this, "Cadastro deletado com sucesso!");
                tfCod.setText("");
                tfName.setText("");
                tfLastname.setText("");
                tfCity.setSelectedIndex(0);
            } else {
                JOptionPane.showMessageDialog(this, "Erro ao tentar deletar!");
            }
        }
    }//GEN-LAST:event_btDelActionPerformed

    private void showData(StudentEntity student) {
        tfCod.setText(String.valueOf(student.getId()));
        tfName.setText(student.getName());
        tfLastname.setText(student.getLastname());
        tfCity.setSelectedItem(citiesDAO.getByID(student.getCityID()));
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Student dialog = new Student(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btDel;
    private javax.swing.JButton btFirst;
    private javax.swing.JButton btLast;
    private javax.swing.JButton btNew;
    private javax.swing.JButton btNext;
    private javax.swing.JButton btPrev;
    private javax.swing.JButton btSave;
    private javax.swing.JButton btSearch;
    private javax.swing.JLabel lCity;
    private javax.swing.JLabel lCod;
    private javax.swing.JLabel lLastname;
    private javax.swing.JLabel lName;
    private javax.swing.JComboBox<String> tfCity;
    private javax.swing.JTextField tfCod;
    private javax.swing.JTextField tfLastname;
    private javax.swing.JTextField tfName;
    // End of variables declaration//GEN-END:variables
}

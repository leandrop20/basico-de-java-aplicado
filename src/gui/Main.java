package gui;

import javax.swing.JFrame;

/**
 *
 * @author Leandro Pinheiro
 */
public class Main extends JFrame {

    private Menu menu;
    
    public Main(String title) {
        super(title);
        
        menu = new Menu(this);
        this.setJMenuBar(menu);
    }
    
    public void menuHandler(String name) {
        switch (name) {
            case "student":
                new Student(this, true).setVisible(true);
                break;
            case "city":
                new City(this, true).setVisible(true);
                break;
            case "exit":
                System.exit(0);
                break;
            case "students":
                new Students(this, true).setVisible(true);
                break;
            case "cities":
                new Cities(this, true).setVisible(true);
                break;
        }
    }
    
    public static void main(String[] args) {
        Main stage = new Main("CDM - Basico Java Aplicado");
        stage.setSize(768, 450);
        stage.setResizable(false);
        stage.setLocationRelativeTo(null);
        stage.setVisible(true);
        stage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
}

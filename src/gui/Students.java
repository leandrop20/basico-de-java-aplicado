package gui;

import dao.CitiesDAO;
import dao.StudentsDAO;
import entities.StudentEntity;
import java.util.ArrayList;

/**
 *
 * @author MN-LEO
 */
public class Students extends javax.swing.JDialog {

    StudentsDAO studentsDAO;
    CitiesDAO citiesDAO;

    public Students(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(parent);

        studentsDAO = new StudentsDAO();
        citiesDAO = new CitiesDAO();
        String nameCity;
        ArrayList<StudentEntity> students = studentsDAO.getAllStudents();
        for (int i = 0; i < students.size(); i++) {
            tStudents.setValueAt(students.get(i).getId(), i, 0);
            tStudents.setValueAt(students.get(i).getName(), i, 1);
            tStudents.setValueAt(students.get(i).getLastname(), i, 2);
            nameCity = citiesDAO.getByID(students.get(i).getCityID()).getName();
            tStudents.setValueAt(nameCity, i, 3);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tStudents = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        tStudents.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Nome", "Sobrenome", "Cidade"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tStudents.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tStudents);
        if (tStudents.getColumnModel().getColumnCount() > 0) {
            tStudents.getColumnModel().getColumn(0).setResizable(false);
            tStudents.getColumnModel().getColumn(0).setPreferredWidth(30);
            tStudents.getColumnModel().getColumn(1).setResizable(false);
            tStudents.getColumnModel().getColumn(2).setResizable(false);
            tStudents.getColumnModel().getColumn(3).setResizable(false);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Students dialog = new Students(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tStudents;
    // End of variables declaration//GEN-END:variables
}

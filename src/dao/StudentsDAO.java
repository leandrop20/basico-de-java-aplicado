package dao;

import entities.StudentEntity;
import factory.ConnectDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author MN-LEO
 */
public class StudentsDAO {

    private Connection con;
    private Statement stmt;
    private ResultSet rs;
    private Statement stmtNavigator;
    private ResultSet rsNavigator;

    public StudentsDAO() {
        con = ConnectDB.getConnection();

        try {
            stmtNavigator = con.createStatement();
            rsNavigator = stmtNavigator.executeQuery("SELECT * FROM alunos");
        } catch (SQLException e) {
            System.out.println("error: " + e);
        }
    }

    public ArrayList<StudentEntity> getAllStudents() {
        String sql = "SELECT * FROM alunos";
        ArrayList<StudentEntity> out = new ArrayList();
        StudentEntity student;
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                student = new StudentEntity();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("nome"));
                student.setLastname(rs.getString("sobrenome"));
                student.setCityID(rs.getInt("cidade_id"));

                out.add(student);
            }
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        }

        return out;
    }

    public StudentEntity getByID(int id) {
        String sql = "SELECT * FROM alunos WHERE id=" + id;
        StudentEntity student = null;

        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                student = new StudentEntity();

                student.setId(rs.getInt("id"));
                student.setName(rs.getString("nome"));
                student.setLastname(rs.getString("sobrenome"));
                student.setCityID(rs.getInt("cidade_id"));
            }

            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        }

        return student;
    }

    public int save(StudentEntity student) {
        try {
            stmt = con.createStatement();

            int result = stmt.executeUpdate("INSERT INTO alunos SET nome='" + student.getName() + "', sobrenome='" + student.getLastname() + "', cidade_id=" + student.getCityID());

            return result;
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        }

        return 0;
    }

    public int update(StudentEntity student) {
        try {
            stmt = con.createStatement();

            int result = stmt.executeUpdate("UPDATE alunos SET nome='" + student.getName() + "', sobrenome='" + student.getLastname() + "', cidade_id=" + student.getCityID() + " WHERE id=" + student.getId());

            return result;
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        }

        return 0;
    }

    public int delete(int id) {
        try {
            stmt = con.createStatement();

            int result = stmt.executeUpdate("DELETE FROM alunos WHERE id=" + id);
            
            return result;
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        }

        return 0;
    }

    public StudentEntity nav(String type) {
        try {
            switch (type) {
                case "first":
                    if (rsNavigator.first()) {
                        return getStudent();
                    }
                    break;
                case "last":
                    if (rsNavigator.last()) {
                        return getStudent();
                    }
                    break;
                case "prev":
                    if (!rsNavigator.isFirst()) {
                        rsNavigator.previous();
                        return getStudent();
                    }
                    break;
                case "next":
                    if (!rsNavigator.isLast()) {
                        rsNavigator.next();
                        return getStudent();
                    }
                    break;
            }

        } catch (SQLException e) {
            System.out.println("error: " + e);
        }

        return null;
    }

    public StudentEntity getStudent() {
        try {
            StudentEntity student = new StudentEntity();
            student.setId(rsNavigator.getInt("id"));
            student.setName(rsNavigator.getString("nome"));
            student.setLastname(rsNavigator.getString("sobrenome"));
            student.setCityID(rsNavigator.getInt("cidade_id"));

            return student;
        } catch (SQLException e) {
            System.out.println("error: " + e);
        }

        return null;
    }
}

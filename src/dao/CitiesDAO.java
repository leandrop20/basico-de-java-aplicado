package dao;

import entities.CityEntity;
import entities.StudentEntity;
import factory.ConnectDB;
import gui.City;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author MN-LEO
 */
public class CitiesDAO {

    private Connection con;
    private Statement stmt;
    private ResultSet rs;
    private Statement stmtNavigator;
    private ResultSet rsNavigator;

    public CitiesDAO() {
        con = ConnectDB.getConnection();
        
        try {
            stmtNavigator = con.createStatement();
            rsNavigator = stmtNavigator.executeQuery("SELECT * FROM cidades");
        } catch (SQLException e) {
            System.out.println("error: " + e);
        }
    }

    public ArrayList<CityEntity> getAllCities() {
        String sql = "SELECT * FROM cidades";
        ArrayList<CityEntity> out = new ArrayList();
        CityEntity city;
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                city = new CityEntity();
                city.setId(rs.getInt("id"));
                city.setName(rs.getString("nome"));
                city.setState(rs.getString("estado"));
                
                out.add(city);
            }
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        }

        return out;
    }

    public CityEntity getByID(int id) {
        String sql = "SELECT * FROM cidades WHERE id=" + id;
        CityEntity city = null;

        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                city = new CityEntity();
                city.setId(rs.getInt("id"));
                city.setName(rs.getString("nome"));
                city.setState(rs.getString("estado"));
            }
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        }

        return city;
    }

    public int getIDByName(String name) {
        String sql = "SELECT * FROM cidades WHERE nome='" + name + "'";
        int out = 0;

        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                out = rs.getInt("id");
            }
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        }
        return out;
    }

    public int save(CityEntity city) {
        try {
            stmt = con.createStatement();
            int result = stmt.executeUpdate("INSERT INTO cidades SET nome='" + city.getName() + "', estado='" + city.getState() + "'");

            return result;
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        }

        return 0;
    }

    public int update(CityEntity city) {
        try {
            stmt = con.createStatement();
            int result = stmt.executeUpdate("UPDATE cidades SET nome='" + city.getName() + "', estado='" + city.getState() + "' WHERE id=" + city.getId());

            return result;
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        }

        return 0;
    }

    public int delete(int id) {
        try {
            stmt = con.createStatement();

            int result = stmt.executeUpdate("DELETE FROM cidades WHERE id=" + id);
            
            return result;
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        }
        
        return 0;
    }
    
    public CityEntity nav(String type) {
        try {
            switch (type) {
                case "first":
                    if (rsNavigator.first()) {
                        return getCity();
                    }
                    break;
                case "last":
                    if (rsNavigator.last()) {
                        return getCity();
                    }
                    break;
                case "prev":
                    if (!rsNavigator.isFirst()) {
                        rsNavigator.previous();
                        return getCity();
                    }
                    break;
                case "next":
                    if (!rsNavigator.isLast()) {
                        rsNavigator.next();
                        return getCity();
                    }
                    break;
            }

        } catch (SQLException e) {
            System.out.println("error: " + e);
        }

        return null;
    }

    public CityEntity getCity() {
        try {
            CityEntity city = new CityEntity();
            city.setId(rsNavigator.getInt("id"));
            city.setName(rsNavigator.getString("nome"));
            city.setState(rsNavigator.getString("estado"));

            return city;
        } catch (SQLException e) {
            System.out.println("error: " + e);
        }

        return null;
    }
}
